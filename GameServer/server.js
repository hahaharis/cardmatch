var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server, {pingInterval:500, pingTimeout:3600000});

server.listen(3000);

//global constant for the server
var MAX_ROOMS = 2;
var MAX_CONNECTIONS = 2; // max people can connect to a single room

var PLAYER_CONNECT = "player connect";
var PLAYER_UPDATE = "player update";
var ROOM_CREATE = "room create";
var ROOM_UPDATE = "room update";
var ROOM_JOIN = "room join";
var ROOM_LEAVE = "room leave";
var OTHER_PLAYER_JOINED = "other player joined";
var OTHER_PLAYER_LEFT = "other player left";
var PLAY = "play";
var DONE = "done";
var CAN_REVEAL = "can reveal";
var CARD_REVEAL = "card reveal";
var CARD_MATCHED = "card matched";
var DISCONNECT = "disconnect";
var CLIENT_PING = "client ping";

var types = { easy:{ symbols:8, max:2 }, hard:{ symbols:18, max:4 } };
var symbols = ["&", ":)", "#", "%", ":(", "?", "=", "{}", "$"];
var colors = {"#1592F264":null, "#22F21564":null, "#ECFF0064":null, "#FF000064":null, "#D100FF64":null};

// global variables for the server
var clients = {};
var rooms = {};

app.get('/', function(req, res) {
    res.send('Hey yo!');
});

io.on('connection', function(socket){
    var client_room;
    var client_id;
    var currentPlayer = {};
    currentPlayer.name = 'Unknown';

    // ON_PLAYER_CONNECT
    socket.on(PLAYER_CONNECT, function(data){
        if(!data) return;
        console.log(data.name+' recv: player connect');
        client_id = guid();
        currentPlayer = {
            name: data.name,
            id: client_id,
            isPlayer:false
        };
        clients[client_id] = currentPlayer;
        socket.emit(PLAYER_CONNECT, currentPlayer);
        if(Object.keys(rooms).length){
            for (const roomName in rooms) {
                if (rooms.hasOwnProperty(roomName)) {
                    const room = rooms[roomName];
                    socket.emit(ROOM_UPDATE, room);
                }
            }
        }
    });
    
    // ON_ROOM_CREATE
    socket.on(ROOM_CREATE, function(data){
        if(data && !client_room){
            console.log(currentPlayer.name+' recv: player room create: '+JSON.stringify(data));
            var rooms_length = Object.keys(rooms).length;
            var temp = data!=null && data.roomName ? data.roomName : "Room_"+rooms_length;
            if(temp){
                client_room = temp;
                rooms[client_room] = {
                    name: client_room,
                    max: data.type ? types[data.type].max : MAX_CONNECTIONS,
                    players: [client_id],
                    type: data.type ? data.type : 'easy',
                    colors: colors
                };
                currentPlayer.isPlayer = true;
                for (const key in rooms[client_room].colors) {
                    if (!rooms[client_room].colors[key]) {
                        currentPlayer.color = key;
                        rooms[client_room].colors[key] = currentPlayer.id;
                        break;
                    }
                }
                clients[client_id] = currentPlayer;
                socket.join(client_room);
                
                var playerDetails = [];
                for (var i = 0; i < rooms[client_room].players.length; i++) {
                    var index = rooms[client_room].players[i];
                    playerDetails.push(clients[index]);
                }
                var room = {
                    name: rooms[client_room].name,
                    max: rooms[client_room].max,
                    type: rooms[client_room].type,
                    players: rooms[client_room].players,
                    playerDetails: playerDetails
                }
                
                socket.emit(ROOM_CREATE, room);
                socket.broadcast.emit(ROOM_UPDATE, room);
            }
        }
    });

    // ON_ROOM_JOIN
    socket.on(ROOM_JOIN, function(data){
        if(!data) return;
        console.log(currentPlayer.name+' recv: player room join: '+JSON.stringify(data));
        var temp = data.name;
        if(!client_room && temp && rooms[temp]){
            client_room = temp;
            rooms[client_room].players.push(client_id);
            if(rooms[client_room].players.length <= rooms[client_room].max){
                currentPlayer.isPlayer = true;
                for (const key in rooms[client_room].colors) {
                    if (!rooms[client_room].colors[key]) {
                        currentPlayer.color = key;
                        rooms[client_room].colors[key] = currentPlayer.id;
                        break;
                    }
                }
            }
            clients[client_id] = currentPlayer;
            socket.join(client_room);
            
            var playerDetails = [];
            for (var i = 0; i < rooms[client_room].players.length; i++) {
                var index = rooms[client_room].players[i];
                playerDetails.push(clients[index]);
            }
            var room = {
                name: rooms[client_room].name,
                max: rooms[client_room].max,
                type: rooms[client_room].type,
                players: rooms[client_room].players,
                playerDetails: playerDetails,
                game: rooms[client_room].game
            }

            socket.emit(ROOM_JOIN, room);
            socket.broadcast.emit(ROOM_UPDATE, rooms[client_room]);
            socket.broadcast.to(client_room).emit(OTHER_PLAYER_JOINED, currentPlayer);
        }
    });

    // ON_ROOM_LEAVE
    socket.on(ROOM_LEAVE, function(){
        if(client_room){
            console.log(currentPlayer.name+' recv: player room leave: '+client_room);
            socket.leave(client_room);
            currentPlayer.isPlayer = false;
            if(rooms[client_room] != null) {
                var index = rooms[client_room].players.indexOf(client_id);
                if (index !== -1) rooms[client_room].players.splice(index, 1);
            }
            socket.emit(ROOM_LEAVE);
            socket.broadcast.to(client_room).emit(OTHER_PLAYER_LEFT, currentPlayer);
            
            // shift users to be player
            rooms[client_room].colors[currentPlayer.color] = null;
            currentPlayer.color = null;
            for (var i = 0; i < rooms[client_room].players.length; i++) {
                var id = rooms[client_room].players[i];
                if(i < rooms[client_room].max && !clients[id].isPlayer) {
                    clients[id].isPlayer = true;
                    for (const key in rooms[client_room].colors) {
                        if (!rooms[client_room].colors[key]){
                            clients[id].color = key;
                            rooms[client_room].colors[key] = id;
                            break;
                        }
                    }
                    socket.broadcast.to(client_room).emit(PLAYER_UPDATE, clients[id]);
                }
            }

            var room = rooms[client_room];
            if(rooms[client_room].players.length<=0){
                room = { name:client_room, deleted:true };
                delete rooms[client_room];
            }

            socket.emit(ROOM_UPDATE, room);
            socket.broadcast.emit(ROOM_UPDATE, room);
            client_room = null;
        }
    });

    // ON_PLAY
    socket.on(PLAY, function(){
        if(client_room && currentPlayer.isPlayer && rooms[client_room] &&
           (!rooms[client_room].game || !rooms[client_room].game.isPlaying)){

            console.log(currentPlayer.name+' recv: player start the game');
            var game = {};
            game.isPlaying = true;
            rooms[client_room].game = game;
            game.room = client_room;
            
            // randomize symbols
            game.total = types[rooms[client_room].type].symbols; // total symbol per game
            game.symbols = randomizeSymbols(game.total);

            game.matching = [];
            game.matched = [];

            rooms[client_room].game = game;
            socket.emit(PLAY, game);
            socket.broadcast.to(client_room).emit(PLAY, game);
        }
    });
    
    // ON_CARD_REVEAL
    socket.on(CARD_REVEAL, function(data){
        if(data && client_room && currentPlayer.isPlayer && rooms[client_room] &&
           rooms[client_room].game && rooms[client_room].game.isPlaying &&
           rooms[client_room].game.matching.length < 2){

            console.log(currentPlayer.name+' recv: player reveal card: '+JSON.stringify(data));

            var game = rooms[client_room].game;
            game.matching.push.apply(game.matching, [data.id]);
            if(game.matching.length > 2) {
                game.matching.pop();
            } else {
                // first let other know a card reveal
                data.color = currentPlayer.color;
                socket.emit(CARD_REVEAL, data);
                socket.broadcast.to(client_room).emit(CARD_REVEAL, data);
            }
            rooms[client_room].game = game;

            // then check if matching
            if(game.matching.length >= 2) {
                setTimeout(function(){
                    var isMatched = false;
                    var firstSymbols = game.symbols[game.matching[0]];
                    var secondSymbols = game.symbols[game.matching[1]];
                    if(firstSymbols == secondSymbols){
                        isMatched = true;
                        game.matched.push(game.matching[0],game.matching[1]);
                        
                        if(game.matched.length >= game.total) {
                            rooms[client_room].game.isPlaying = false;
                            setTimeout(function(){
                                socket.emit(DONE);
                                socket.broadcast.to(client_room).emit(DONE);
                            }, 100);
                        }
                    }
                    var matched = { card1:game.matching[0], card2:game.matching[1],  matched:isMatched };
                    socket.emit(CARD_MATCHED, matched);
                    socket.broadcast.to(client_room).emit(CARD_MATCHED, matched);

                    game.matching = [];
                    rooms[client_room].game = game;
                }, 500);
            }
        }
    });

    // ON_DISCONNECT
    socket.on(DISCONNECT, function(){
        console.log(currentPlayer.name+' recv: disconnect '+currentPlayer.name);
        if(client_room){ // similiar with ROOM_LEAVE
            console.log(currentPlayer.name+' recv: player room leave: '+client_room);
            socket.leave(client_room);
            currentPlayer.isPlayer = false;
            if(rooms[client_room] != null) {
                var index = rooms[client_room].players.indexOf(client_id);
                if (index !== -1) rooms[client_room].players.splice(index, 1);
            }
            socket.emit(ROOM_LEAVE);
            socket.broadcast.to(client_room).emit(OTHER_PLAYER_LEFT, clients[client_id]);

            // shift users to be player
            rooms[client_room].colors[currentPlayer.color] = null;
            currentPlayer.color = null;
            for (var i = 0; i < rooms[client_room].players.length; i++) {
                var id = rooms[client_room].players[i];
                if(i < rooms[client_room].max && !clients[id].isPlayer) {
                    clients[id].isPlayer = true;
                    for (const key in rooms[client_room].colors) {
                        if (!rooms[client_room].colors[key]) {
                            clients[id].color = key;
                            rooms[client_room].colors[key] = id;
                            break;
                        }
                    }
                    socket.broadcast.to(client_room).emit(PLAYER_UPDATE, clients[id]);
                }
            }

            var room = rooms[client_room];
            if(rooms[client_room].players.length<=0){
                room = { name:client_room, deleted:true };
                delete rooms[client_room];
            }

            socket.emit(ROOM_UPDATE, room);
            socket.broadcast.emit(ROOM_UPDATE, room);

            client_room = null;
        }
        delete clients[client_id];
    });

    // ON_PING
    socket.on(CLIENT_PING, function(){});

});

console.log('--- server is running...');


function guid(){
    function s4(){
        return Math.floor((1+Math.random()) * 0x10000).toString(16).substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function randomizeSymbols(total){
    var array = total > 8 ? symbols.slice(0) : symbols.slice(0, (total/2));
    array.push.apply(array, array);
    for (var i = 0; i < array.length; i++) {
        var temp = array[i];
        var r = Math.floor(Math.random() * ((array.length-1) - i) ) + i;
        array[i] = array[r];
        array[r] = temp;
    }
    return array;
}
