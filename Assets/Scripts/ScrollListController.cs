﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollListController : MonoBehaviour {

	[SerializeField] private GameObject tempRoomInfo;
	[SerializeField] private GameObject noRoomText;

	[SerializeField] private int[] intArray;

	private List<GameObject> RoomInfoList = new List<GameObject>();

	public void GenerateAll() {
		RoomInfoList = new List<GameObject>();
		if(RoomInfoList.Count > 0) {
			foreach(GameObject roomInfo in RoomInfoList){
				Destroy(roomInfo.gameObject);
			}
			RoomInfoList.Clear();
		}

		foreach (int i in intArray){
			GameObject roomInfo = Instantiate(tempRoomInfo) as GameObject;
			roomInfo.SetActive(true);
			// roomInfo.GetComponent<RoomInfo>().SetRoom("Room "+i, i);
			roomInfo.transform.SetParent(tempRoomInfo.transform.parent, false);

		}
	}

	public void Generate(NetworkManager.RoomInfoJSON room) {
		GameObject roomInfo = GameObject.Find(room.name);
		if(roomInfo == null) {
			roomInfo = Instantiate(tempRoomInfo) as GameObject;
			roomInfo.name = room.name;
		}
		roomInfo.SetActive(true);
		roomInfo.GetComponent<RoomInfo>().SetRoom(room.name, room.type, room.max, room.players.Count);
		roomInfo.transform.SetParent(tempRoomInfo.transform.parent, false);
		noRoomText.SetActive(false);
	}

	public void Remove(NetworkManager.RoomInfoJSON room) {
		GameObject roomInfo = GameObject.Find(room.name);
		if(roomInfo != null) {
			RoomInfoList.Remove(roomInfo);
			Destroy(roomInfo);
			if(RoomInfoList.Count <= 0)
				noRoomText.SetActive(true);
		}
	}
	
}
