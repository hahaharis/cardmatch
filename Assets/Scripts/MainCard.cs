﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainCard : MonoBehaviour {

	[SerializeField] private SceneController controller;
	[SerializeField] private GameObject cardBack;
	[SerializeField] private GameObject cardHighlight;
	[SerializeField] private TextMesh symbolText;

	public void OnMouseDown(){
		// if(cardBack.activeSelf && controller.canReveal){
		// 	cardBack.SetActive(false);
		// 	controller.CardRevealed(this);
		// }
		NetworkManager.instance.GetComponent<NetworkManager>().CommandCardReveal(this);
	}

	private int _id;
	public int id {
		get { return _id; }
	}

	private string _symbol;
	public string symbol {
		get { return _symbol; }
	}

	public void ChangeSymbol(int id, string symbol){
		_id = id;
		_symbol = symbol;
		symbolText.text = symbol;
		this.gameObject.name = "Card"+_id;
		select(null);
	}

	public void open(){
		cardBack.SetActive(false);
		select(null);
	}

	public void open(string _color){
		cardBack.SetActive(false);
		select(_color);
	}

	public void hide(){
		cardBack.SetActive(true);
		select(null);
	}

	public void select(string _color){
		if(_color != null) {
			Color color = new Color();
			ColorUtility.TryParseHtmlString(_color, out color);
			cardHighlight.GetComponent<Image>().color = color;
			cardHighlight.SetActive(true);
		} else {
			cardHighlight.SetActive(false);
			cardHighlight.GetComponent<Image>().color = Color.white;
		}
	}
}
