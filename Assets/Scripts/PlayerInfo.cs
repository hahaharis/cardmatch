﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfo : MonoBehaviour {
	[SerializeField] private Text playerName;
	[SerializeField] private Text indicatorText;

	public void SetInfo(string _playerName, bool _isMe, string _color){
		playerName.text = _playerName.Length > 12 ? 
					_playerName.Substring(0, 12) + "..." : _playerName;;
		if(_isMe) indicatorText.text = "player (you)";
		if(_color != null) {
			Color color = new Color();
			ColorUtility.TryParseHtmlString(_color, out color);
			gameObject.GetComponent<Image>().color = color;
		}
		gameObject.SetActive(true);
	}
}
