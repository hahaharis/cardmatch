﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpectatorListController : MonoBehaviour {

	[SerializeField] private GameObject tempSpectatorInfo;
	private List<GameObject> SpectatorInfoList = new List<GameObject>();

	public void Generate(NetworkManager.PlayerJSON _player) {
		GameObject spectatorInfo = GameObject.Find("Spectator-"+_player.id);
		if(spectatorInfo == null) {
			spectatorInfo = Instantiate(tempSpectatorInfo) as GameObject;
			spectatorInfo.name = "Spectator-"+_player.id;
		}
		string name = _player.name.Length > 8 ? _player.name.Substring(0, 8) + "..." : _player.name;
		spectatorInfo.GetComponent<Text>().text = name;
		spectatorInfo.transform.SetParent(tempSpectatorInfo.transform.parent, false);
		spectatorInfo.SetActive(true);
		SpectatorInfoList.Add(spectatorInfo);
	}

	public void Remove(string _id){
		GameObject spectatorInfo = GameObject.Find("Spectator-"+_id);
		if(spectatorInfo != null) {
			Destroy(spectatorInfo);
			SpectatorInfoList.Remove(spectatorInfo);
		}
	}

	public void RemoveAll(){
		if(SpectatorInfoList.Count > 0)
			foreach (GameObject spectatorInfo in SpectatorInfoList) {
				Destroy(spectatorInfo);	
			}
	}
}
