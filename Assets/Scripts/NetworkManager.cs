﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SocketIO;

public class NetworkManager : MonoBehaviour {

	private const string PLAYER_CONNECT = "player connect";
	private const string PLAYER_UPDATE = "player update";
	private const string ROOM_CREATE = "room create";
	private const string ROOM_UPDATE = "room update";
	private const string ROOM_JOIN = "room join";
	private const string ROOM_LEAVE = "room leave";
	private const string OTHER_PLAYER_JOINED = "other player joined";
	private const string OTHER_PLAYER_LEFT = "other player left";
	private const string PLAY = "play";
	private const string DONE = "done";
	private const string CAN_REVEAL = "can reveal";
	private const string CARD_REVEAL = "card reveal";
	private const string CARD_MATCHED = "card matched";
	private const string DISCONNECT = "disconnect";
	private const string CLIENT_PING = "client ping";

	public static NetworkManager instance;
	public SocketIOComponent socket;
	public ScrollListController listController;
	public SceneController sceneController;
	public InputField playerNameInput;
	public Canvas MainUI, MainGame;

	[HideInInspector] public PlayerJSON player;
	
	private void Awake() {;
		if(instance == null)
			instance = this;
	 	else if (instance != null)
			Destroy(gameObject);
		
		DontDestroyOnLoad(gameObject);
	}

	private bool isPaused = false;
	private void OnApplicationFocus(bool hasFocus){
		isPaused = !hasFocus;
	}

	private void OnApplicationQuit(){
		StopAllCoroutines();
		if(socket != null)
			socket.socket.Close();
	}
	

	// Use this for initialization
	void Start () {
		//Subscribe to all the various websocket events
		socket.On(PLAYER_CONNECT, OnPlayerConnect);
		socket.On(PLAYER_UPDATE, OnPlayerUpdate);
		socket.On(OTHER_PLAYER_JOINED, OnOtherPlayerJoined);
		socket.On(OTHER_PLAYER_LEFT, OnOtherPlayerLeft);
		socket.On(ROOM_CREATE, OnRoomCreate);
		socket.On(ROOM_UPDATE, OnRoomUpdate);
		socket.On(ROOM_JOIN, OnRoomJoin);
		socket.On(ROOM_LEAVE, OnRoomLeave);
		socket.On(PLAY, OnPlay);
		socket.On(DONE, OnDone);
		socket.On(CARD_REVEAL, OnCardReveal);
		socket.On(CARD_MATCHED, OnCardMatched);
	}
	
	public void EnterGame() {
		if(playerNameInput.text != "")
			StartCoroutine(ConnectToServer());
	}

	public void DropDownCreateRoom(){
		GameObject dropdown = GameObject.Find("CreateRoomButton").transform.Find("CreateRoomDropDown").gameObject;
		if(dropdown != null)
			dropdown.SetActive(!dropdown.activeSelf);
	}
	
	#region Commands
		
	IEnumerator PingToServer() {
		while(true){
			if(isPaused)
				socket.Emit(CLIENT_PING);
			yield return new WaitForSeconds(5f);
		}
	}	

	IEnumerator ConnectToServer() {
		GameObject.Find("EnterButton").GetComponentInChildren<Text>().text = "Connecting...";
		yield return new WaitForSeconds(0.5f);

		string playerName = playerNameInput.text;
		PlayerJSON playerJSON = new PlayerJSON();
		playerJSON.name = playerName;
		string data = JsonUtility.ToJson(playerJSON);
		socket.Emit(PLAYER_CONNECT, new JSONObject(data));

		if(!socket.IsConnected)
			GameObject.Find("EnterButton").GetComponentInChildren<Text>().text = "Can't Connect";
	}

	public void CommandCreateRoom(string type){
		string data = JsonUtility.ToJson(new RoomInfoJSON("", type));
		socket.Emit(ROOM_CREATE, new JSONObject(data));
		DropDownCreateRoom();
	}

	public void CommandRoomJoin(string roomName){
		string data = JsonUtility.ToJson(new RoomInfoJSON(roomName, ""));
		socket.Emit(ROOM_JOIN, new JSONObject(data));
	}

	public void CommandRoomLeave(){
		socket.Emit(ROOM_LEAVE);
	}

	public void CommandPlayGame(){
		socket.Emit(PLAY);
	}

	public void CommandCardReveal(MainCard card){
		string data = JsonUtility.ToJson(new CardJSON(card.name, card.symbol, card.id));
		socket.Emit(CARD_REVEAL, new JSONObject(data));
	}

	#endregion


	#region Listener
	
	void OnPlayerConnect(SocketIOEvent socketIOEvent){
		print("You entered the game");
		string data = socketIOEvent.data.ToString();
		player = PlayerJSON.CreateFromJSON(data);

		MainUI.transform.Find("LoginArea").gameObject.SetActive(false);
		MainUI.transform.Find("WelcomeArea").gameObject.SetActive(true);
		MainUI.transform.Find("ScrollList").gameObject.SetActive(true);
		MainUI.transform.Find("WelcomeArea").gameObject.transform.Find("PlayerName").GetComponent<Text>().text = player.name;

		// too keep connection alive
		StartCoroutine(PingToServer());
	}

	void OnPlayerUpdate(SocketIOEvent socketIOEvent){
		print("You entered the game");
		string data = socketIOEvent.data.ToString();
		PlayerJSON _player = PlayerJSON.CreateFromJSON(data);
		sceneController.PlayerUpdate(_player);
		if(player.id == _player.id)
			player = _player;
	}
	
	void OnRoomUpdate(SocketIOEvent socketIOEvent){
		// only update room if MainUI active
		if(MainUI.gameObject.activeSelf && MainUI.transform.Find("ScrollList").gameObject.activeSelf) {
			print("Receive room update");
			string data = socketIOEvent.data.ToString();
			RoomInfoJSON roomInfo = RoomInfoJSON.CreateFromJSON(data);
			if(roomInfo.deleted)
				listController.Remove(roomInfo);
			else
				listController.Generate(roomInfo);
		}
	}
	
	void OnRoomCreate(SocketIOEvent socketIOEvent){
		print("You've created and joined a room!");
		player.isPlayer = true;
		string data = socketIOEvent.data.ToString();
		RoomInfoJSON roomInfo = RoomInfoJSON.CreateFromJSON(data);
		listController.Generate(roomInfo);

		sceneController.SetUp(player, roomInfo);
		MainUI.gameObject.SetActive(false);
	}
	
	void OnRoomJoin(SocketIOEvent socketIOEvent){
		print("You joined a room!");
		string data = socketIOEvent.data.ToString();
		RoomInfoJSON roomInfo = RoomInfoJSON.CreateFromJSON(data);
		listController.Generate(roomInfo);

		sceneController.SetUp(player, roomInfo);
		MainUI.gameObject.SetActive(false);
	}

	void OnRoomLeave(SocketIOEvent socketIOEvent){
		print("You left the room.");
		player.isPlayer = false;
		MainUI.gameObject.SetActive(true);
		sceneController.Clear();
	}
	
	void OnOtherPlayerJoined(SocketIOEvent socketIOEvent){
		print("Someone has joined the room");
		string data = socketIOEvent.data.ToString();
		PlayerJSON playerJSON = PlayerJSON.CreateFromJSON(data);
		sceneController.PlayerJoin(playerJSON);
	}

	void OnOtherPlayerLeft(SocketIOEvent socketIOEvent){
		print("User disconnected!");
		string data = socketIOEvent.data.ToString();
		PlayerJSON playerJSON = PlayerJSON.CreateFromJSON(data);
		sceneController.PlayerLeft(playerJSON);
	}
	
	void OnPlay(SocketIOEvent socketIOEvent){
		print("The game has started!");
		string data = socketIOEvent.data.ToString();
		sceneController.PlayGame(GameInfoJSON.CreateFromJSON(data));
	}
	
	void OnDone(SocketIOEvent socketIOEvent){
		print("The game has finished!");
		sceneController.EndGame();
	}

	void OnCardReveal(SocketIOEvent socketIOEvent){
		print("A card revealed!");
		string data = socketIOEvent.data.ToString();
		CardJSON card = CardJSON.CreateFromJSON(data);
		sceneController.RevealCard(card.name, card.color);
	}

	void OnCardMatched(SocketIOEvent socketIOEvent){
		print("A card matched result recieved.");
		string data = socketIOEvent.data.ToString();
		CardMatchedJSON card = CardMatchedJSON.CreateFromJSON(data);
		if(!card.matched)
			sceneController.CardNotMatched(card.card1, card.card2);
	}

	#endregion


	#region JSONMessageClasses
	
	[Serializable]
	public class GameInfoJSON {
		public bool isPlaying;
		public int total; // total symbols
		public string[] symbols;
		public int[] matched;
		public int[] matching;

		public static GameInfoJSON CreateFromJSON(string data){
			return JsonUtility.FromJson<GameInfoJSON>(data);
		}
	}

	[Serializable]
	public class RoomInfoJSON {
		public string name;
		public string type; // game type
		public int max;
		public List<string> players;
		public List<PlayerJSON> playerDetails;
		public GameInfoJSON game;
		public bool deleted;

		public RoomInfoJSON(string _name, string _type){
			name = _name;
			type = _type;	
		}

		public static RoomInfoJSON CreateFromJSON(string data){
			return JsonUtility.FromJson<RoomInfoJSON>(data);
		}
	}

	[Serializable]
	public class PlayerJSON {
		public string name;
		public string id;
		public bool isPlayer;
		public string color;

		public static PlayerJSON CreateFromJSON(string data){
			return JsonUtility.FromJson<PlayerJSON>(data);
		}
	}

	[Serializable]
	public class CardJSON {
		public string name;
		public string symbol;
		public int id;
		public string color;

		public CardJSON(string _name, string _symbol, int _id){
			name = _name;
			symbol = _symbol;
			id = _id;			
		}

		public static CardJSON CreateFromJSON(string data){
			return JsonUtility.FromJson<CardJSON>(data);
		}
	}

	[Serializable]
	public class CardMatchedJSON {
		public int card1;
		public int card2;
		public bool matched;

		public static CardMatchedJSON CreateFromJSON(string data){
			return JsonUtility.FromJson<CardMatchedJSON>(data);
		}
	}
		
	#endregion
}
