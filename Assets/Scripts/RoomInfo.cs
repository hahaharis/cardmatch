﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomInfo : MonoBehaviour {

	[SerializeField] public Text RoomName;
	[SerializeField] public Text RoomPlayers;
	[SerializeField] public Text ButtonText;
	[HideInInspector] public string room_name;

	public void SetRoom(string _roomName, string type, int max, int players){
		RoomName.text = _roomName + " (" + type + ")";
		RoomPlayers.text = (players > max ? max : players) + "/" + max + " players";
		ButtonText.text = players >= max ? "Spectate" : "Join";
		room_name = _roomName;
	}

	public void OnClick(){ 
		NetworkManager.instance.GetComponent<NetworkManager>().CommandRoomJoin(room_name);
	}

}
