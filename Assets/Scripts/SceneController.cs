﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SceneController : MonoBehaviour {

	private int gridRows = 2;
	private int gridCols = 4;
	private float offsetX = 5.0f;
	private float offsetY = 7.5f;

	[SerializeField] private MainCard easyTempCard;
	[SerializeField] private MainCard hardTempCard;
	private List<GameObject> CardList = new List<GameObject>();

	[SerializeField] private GameObject playButton;
	[SerializeField] private GameObject tempPlayerInfo;
	private List<GameObject> PlayerInfoList = new List<GameObject>();

	[SerializeField] private SpectatorListController spectatorListController;

	private string[] symbols = {"&", ":)", "#", "%", ":(", "?", "=", "{}", "[]"};
	private NetworkManager.RoomInfoJSON roomInfo;
	private NetworkManager.PlayerJSON currentPlayer;

	public void SetUp(NetworkManager.PlayerJSON _currentPlayer, NetworkManager.RoomInfoJSON _roomInfo){
		roomInfo = _roomInfo;
		currentPlayer = _currentPlayer;

		Clear();

		if(roomInfo.game!=null && roomInfo.game.isPlaying) {
			PlayGame(roomInfo.game);
			// check for already mathced cards
			for (int i = 0; i < roomInfo.game.matched.Length; i++){
				GameObject card = GameObject.Find("Card"+roomInfo.game.matched[i]);
				if(card != null) card.GetComponent<MainCard>().open(); 
			}
			// check for open cards
			for (int i = 0; i < roomInfo.game.matching.Length; i++){
				GameObject card = GameObject.Find("Card"+roomInfo.game.matching[i]);
				if(card != null) card.GetComponent<MainCard>().open(); 
			}
		}

		for (int i = 0; i < roomInfo.playerDetails.Count; i++){
			if(roomInfo.playerDetails[i].isPlayer){
				GameObject playerInfo = GameObject.Find("PlayerInfo-" + roomInfo.playerDetails[i].id);
				if(playerInfo == null) {
					playerInfo = Instantiate(tempPlayerInfo) as GameObject;
					playerInfo.name = "PlayerInfo-" + roomInfo.playerDetails[i].id;
				}
				playerInfo.transform.SetParent(tempPlayerInfo.transform.parent, false);
				playerInfo.GetComponent<PlayerInfo>().SetInfo(roomInfo.playerDetails[i].name, 
					roomInfo.playerDetails[i].id == currentPlayer.id, roomInfo.playerDetails[i].color);
				if(roomInfo.playerDetails[i].id == currentPlayer.id)
					currentPlayer = roomInfo.playerDetails[i];
				PlayerInfoList.Add(playerInfo);
			} else
				spectatorListController.Generate(roomInfo.playerDetails[i]);
		}

		CheckCanPlay();
	}

	public void Clear(){
		playButton.transform.Find("DoneText").gameObject.SetActive(false);

		if(PlayerInfoList.Count > 0) {
			foreach(GameObject playerInfo in PlayerInfoList){
				Destroy(playerInfo);
			}
			PlayerInfoList = new List<GameObject>();
		}
		if(CardList.Count > 0) {
			foreach(GameObject card in CardList){
				Destroy(card);
			}
			CardList = new List<GameObject>();
		}

		spectatorListController.RemoveAll();
	}

	bool CheckCanPlay(){
		if(roomInfo.players.Count >= roomInfo.max && currentPlayer.isPlayer){
			playButton.GetComponentInChildren<Text>().text = "Play";
			playButton.GetComponent<Image>().color = new Color(1f,1f,1f,1f);
			return true;
		} else {
			playButton.GetComponentInChildren<Text>().text = "Waiting players...";
			playButton.GetComponent<Image>().color = new Color(1f,1f,1f,0.3f);
			return false;
		}
	}

	public void PlayerJoin(NetworkManager.PlayerJSON _playerJoin){
		if(!roomInfo.playerDetails.Contains(_playerJoin)){
			roomInfo.playerDetails.Add(_playerJoin);
			roomInfo.players.Add(_playerJoin.id);
		}

		if(_playerJoin.isPlayer) {
			GameObject playerInfo = GameObject.Find("PlayerInfo-" + _playerJoin.id);
			if(playerInfo == null) {
				playerInfo = Instantiate(tempPlayerInfo) as GameObject;
				playerInfo.name = "PlayerInfo-" + _playerJoin.id;
			}
			playerInfo.transform.SetParent(tempPlayerInfo.transform.parent, false);
			playerInfo.GetComponent<PlayerInfo>().SetInfo(_playerJoin.name, _playerJoin.id == currentPlayer.id, _playerJoin.color);
			PlayerInfoList.Add(playerInfo);
		} else
			spectatorListController.Generate(_playerJoin);

		CheckCanPlay();
	}

	public void PlayerLeft(NetworkManager.PlayerJSON _playerLeft){
		roomInfo.playerDetails.Remove(_playerLeft);
		roomInfo.players.Remove(_playerLeft.id);
		GameObject playerInfo = GameObject.Find("PlayerInfo-"+_playerLeft.id);
		if(playerInfo != null)
			Destroy(playerInfo);
		spectatorListController.Remove(_playerLeft.id);

		CheckCanPlay();
	}

	public void PlayerUpdate(NetworkManager.PlayerJSON _playerUpdate){
		if(currentPlayer!=null && currentPlayer.id==_playerUpdate.id)
			currentPlayer = _playerUpdate;

		foreach (NetworkManager.PlayerJSON player in roomInfo.playerDetails){
			if(player.id == _playerUpdate.id) {
				player.isPlayer = _playerUpdate.isPlayer;
				break;
			}
		}
		
		if(_playerUpdate.isPlayer){
			GameObject playerInfo = GameObject.Find("PlayerInfo-" + _playerUpdate.id);
			if(playerInfo == null) {
				playerInfo = Instantiate(tempPlayerInfo) as GameObject;
				playerInfo.name = "PlayerInfo-" + _playerUpdate.id;
			}
			playerInfo.transform.SetParent(tempPlayerInfo.transform.parent, false);
			playerInfo.GetComponent<PlayerInfo>().SetInfo(_playerUpdate.name, _playerUpdate.id == currentPlayer.id, _playerUpdate.color);
			PlayerInfoList.Add(playerInfo);
			spectatorListController.Remove(_playerUpdate.id);
		}
		
		CheckCanPlay();
	}

	public void PlayGame(NetworkManager.GameInfoJSON _gameInfo){
		roomInfo.game = _gameInfo;
		playButton.SetActive(false);

		if(CardList.Count > 0) {
			foreach(GameObject card in CardList){
				Destroy(card.gameObject);
			}
			CardList.Clear();
		}

		// Generate Cards
		gridRows = _gameInfo.total > 8 ? 3 : 2;
		gridCols = _gameInfo.total > 8 ? 6 : 4;
		offsetX = _gameInfo.total > 8 ? 3.3f : 5.0f;
		offsetY = _gameInfo.total > 8 ? 4.3f : 7.5f;
		
		MainCard TempCard = _gameInfo.total > 8 ? hardTempCard : easyTempCard;
		Vector3 startPos = TempCard.transform.position;

		for (int i = 0; i < gridCols; i++)
		{
			for (int j = 0; j < gridRows; j++)
			{
				MainCard card = Instantiate(TempCard) as MainCard;
				
				int id = j * gridCols + i;
				card.ChangeSymbol(id, _gameInfo.symbols[id]);

				float posX = (offsetX * i) + startPos.x;
				float posY = (offsetY * j) + startPos.y;
				card.transform.SetParent(TempCard.transform.parent, false);
				card.transform.position = new Vector3(posX, posY, startPos.z);
				card.gameObject.SetActive(true);

				CardList.Add(card.gameObject);
			}
		}
	}

	public void EndGame(){
		if(CardList.Count > 0) {
			foreach(GameObject card in CardList){
				Destroy(card.gameObject);
			}
			CardList.Clear();
		}
		CheckCanPlay();
		playButton.SetActive(true);
		playButton.transform.Find("DoneText").gameObject.SetActive(true);
	}

	public void OnClick(){
		if(CheckCanPlay())
			NetworkManager.instance.GetComponent<NetworkManager>().CommandPlayGame();
	}

	public void RevealCard(string _name, string _color){
		GameObject.Find(_name).GetComponent<MainCard>().open(_color);
	}

	public void CardNotMatched(int _id1, int _id2){
		GameObject.Find("Card"+_id1).GetComponent<MainCard>().hide();
		GameObject.Find("Card"+_id2).GetComponent<MainCard>().hide();
	}

	// Use this for initialization
	void DummyEasy() {
		Vector3 startPos = easyTempCard.transform.position;
		playButton.SetActive(false);

		int[] numbers = {0, 0, 1, 1, 2, 2, 3, 3};
		numbers = ShuffleArray(numbers);

		for (int i = 0; i < gridCols; i++)
		{
			for (int j = 0; j < gridRows; j++)
			{
				MainCard card = Instantiate(easyTempCard) as MainCard;
				
				int index = j * gridCols + i;
				int id = numbers[index];
				card.ChangeSymbol(index, symbols[id]);

				float posX = (offsetX * i) + startPos.x;
				float posY = (offsetY * j) + startPos.y;
				card.transform.SetParent(easyTempCard.transform.parent, false);
				card.transform.position = new Vector3(posX, posY, startPos.z);
				card.gameObject.SetActive(true);

				CardList.Add(card.gameObject);
			}
		}
	}

	// Use this for initialization
	void DummyHard() {
		Vector3 startPos = hardTempCard.transform.position;

		int[] numbers = {0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8};
		numbers = ShuffleArray(numbers);

		for (int i = 0; i < 6; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				MainCard card = Instantiate(hardTempCard) as MainCard;
				
				int index = j * 6 + i;
				int id = numbers[index];
				card.ChangeSymbol(index, symbols[id]);

				float posX = (3.3f * i) + startPos.x;
				float posY = (4.35f * j) + startPos.y;
				card.transform.SetParent(hardTempCard.transform.parent, false);
				card.transform.position = new Vector3(posX, posY, startPos.z);
				card.gameObject.SetActive(true);

				CardList.Add(card.gameObject);
			}
		}
	}
	
	int[] ShuffleArray(int[] numbers){
		int[] newArray = numbers.Clone() as int[];
		for (int i = 0; i < newArray.Length; i++)
		{
			int temp = newArray[i];
			int r = Random.Range(i, newArray.Length);
			newArray[i] = newArray[r];
			newArray[r] = temp;
		}
		return newArray;
	}

	//------------
	//------------

	private MainCard _firstCard;
	private MainCard _secondCard;
	
	public bool canReveal {
		get { return _secondCard == null; }
	}

	public void CardRevealed(MainCard card){
		if(_firstCard == null)
			_firstCard = card;
		else {
			_secondCard = card;
			StartCoroutine(CheckMatch());
		}
	}

	public IEnumerator CheckMatch(){
		if(_firstCard.id != _secondCard.id){
			yield return new WaitForSeconds(0.3f);

			_firstCard.hide();
			_secondCard.hide();
		}

		_firstCard = null;
		_secondCard = null;
	}
}
